import {observer} from 'mobx-react-lite';
import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {useStores} from '../../../App';

const LoginScreen = () => {
  const {navigationStore, authStore} = useStores().stores;
  const [userNameEmpty, setUserNameEmpty] = useState(false);
  const [passwordEmpty, setPasswordEmpty] = useState(false);

  const onSubmit = () => {
    if (authStore.username === '') {
      setUserNameEmpty(true);
      return;
    }
    if (authStore.password === '') {
      setPasswordEmpty(true);
      return;
    }
    authStore.userLogin();
  };

  return (
    <KeyboardAvoidingView
      style={styles.loginScreen}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <View style={styles.loginScreenContainer}>
        <Image
          style={styles.loginScreenImage}
          source={require('../../commons/images/loginscreen.jpg')}
        />
      </View>
      <View style={styles.inputContainer}>
        <Text style={styles.welcomeText}>Witamy zaloguj się</Text>
        <TextInput
          style={styles.userInput}
          onChangeText={authStore.setUsername}
          value={authStore.username}
          placeholder="UserName"
        />
        {userNameEmpty ? <Text>Wpisz login</Text> : null}
        <TextInput
          style={styles.userInput}
          onChangeText={authStore.setPassword}
          value={authStore.password}
          placeholder="Password"
          secureTextEntry
        />
        {passwordEmpty ? <Text>Wpisz haslo</Text> : null}
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.registerButton}
          onPress={() => {
            navigationStore.navigate('Register');
            authStore.setPassword('');
            authStore.setUsername('');
          }}>
          <Text style={styles.registerButtonText}>Zarejestruj się</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.loginButton} onPress={() => onSubmit()}>
          <Text style={styles.buttonText}>Zaloguj</Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  loginScreen: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: '#48DA9F',
  },
  loginScreenContainer: {
    marginTop: '-150%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '150%',
    height: '150%',
    borderBottomLeftRadius: 1000,
    borderBottomRightRadius: 1000,
    overflow: 'hidden',
  },
  loginScreenImage: {
    marginTop: '80%',
    width: '100%',
    height: '50%',
  },
  welcomeText: {
    fontSize: 24,
    color: 'black',
    marginBottom: 15,
  },
  inputContainer: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },
  userInput: {
    backgroundColor: 'white',
    width: 240,
    height: 35,
    marginBottom: 10,
    borderRadius: 20,
    opacity: 0.7,
    padding: 10,
    elevation: 10,
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '70%',
    justifyContent: 'space-evenly',
    marginTop: 10,
  },
  loginButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 35,
    backgroundColor: 'white',
    borderRadius: 20,
    opacity: 0.8,
    elevation: 10,
  },
  registerButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    height: 35,
    borderRadius: 20,
    borderColor: 'white',
    borderWidth: 1,
  },
  registerButtonText: {
    color: 'white',
    fontWeight: '600',
  },
  buttonText: {
    color: 'black',
    fontWeight: '600',
  },
});

export default observer(LoginScreen);
