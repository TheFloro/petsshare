import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  KeyboardAvoidingView,
  Platform,
  ImageBackground,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {useStores} from '../../../App';

const RegisterScreen = () => {
  const {
    authStore,
    authStore: {userLogin},
  } = useStores().stores;
  return (
    <KeyboardAvoidingView
      style={styles.registerScreen}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <ImageBackground
        source={require('../../commons/images/loginscreen.jpg')}
        resizeMode="cover"
        style={styles.image}>
        <View style={styles.registrationContainer}>
          <Text style={styles.registrationTitle}>Rejestracja</Text>
          <View style={styles.userName}>
            <TextInput style={styles.userNameInput} placeholder="Name" />
            <TextInput style={styles.userNameInput} placeholder="Surname" />
          </View>
          <TextInput
            style={styles.userInput}
            placeholder="UserName"
            onChangeText={authStore.setUsername}
            value={authStore.username}
          />
          <TextInput style={styles.userInput} placeholder="Telephone" />
          <TextInput style={styles.userInput} placeholder="E-mail" />
          <TextInput
            style={styles.userInput}
            placeholder="Password"
            onChangeText={authStore.setPassword}
            value={authStore.password}
            secureTextEntry
          />
          <TextInput
            style={styles.userInput}
            placeholder="Repeat Password"
            secureTextEntry
          />
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.registerButton}
              onPress={() => userLogin()}>
              <Text style={styles.buttonText}>Zarejestruj</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  registerScreen: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  registrationTitle: {
    fontSize: 60,
    fontWeight: '800',
    marginBottom: 50,
    marginTop: 10,
    color: 'black',
  },
  registrationContainer: {
    flex: 1,
    backgroundColor: 'rgba(72,218,156,0.5)',
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  userName: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  userNameInput: {
    backgroundColor: 'white',
    width: 120,
    height: 35,
    marginBottom: 10,
    borderRadius: 20,
    padding: 10,
    opacity: 1,
    elevation: 10,
  },
  userInput: {
    backgroundColor: 'white',
    width: 240,
    height: 35,
    marginBottom: 10,
    borderRadius: 20,
    padding: 10,
    opacity: 1,
    elevation: 10,
  },
  avatarTitle: {
    fontSize: 23,
    fontWeight: '800',
    marginBottom: 5,
    marginTop: 10,
    color: 'black',
  },
  imagePicker: {
    width: 100,
    height: 100,
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    elevation: 10,
  },
  imagePickerPlaceholder: {
    fontSize: 70,
    fontWeight: '600',
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '70%',
    justifyContent: 'space-evenly',
    marginTop: 30,
  },
  registerButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 150,
    height: 40,
    backgroundColor: 'white',
    borderRadius: 20,
    opacity: 0.7,
    elevation: 10,
  },
  buttonText: {
    fontSize: 18,
    color: 'black',
    fontWeight: '600',
  },
});

export default RegisterScreen;
