import {observer} from 'mobx-react-lite';
import React, {useState} from 'react';
import {
  ScrollView,
  Text,
  Touchable,
  TouchableOpacity,
  View,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {useStores} from '../../../../App';
import homeStyles from './styles/profileViewStyles';

const ProfileView = () => {
  const {advertismentStore} = useStores().stores;
  const [state, setstate] = useState([false, false, false, false, false]);
  const raiting = (clickedIndex: number) => {
    const table = state.map((x, index) => {
      if (index <= clickedIndex) {
        return true;
      }
      return false;
    });
    console.log(table);
    setstate(table);
  };
  const RaintingStars = () => {
    const array = [];
    for (let i = 0; i < 5; i++) {
      array.push(
        <FontAwesome
          name={state[i] ? 'star' : 'star-o'}
          size={30}
          color="#fcd874"
          key={i}
          onPress={() => raiting(i)}
        />,
      );
    }
    return array;
  };
  return (
    <ScrollView style={homeStyles.container}>
      <View style={homeStyles.userHeader}>
        <View style={homeStyles.userPhoto}>
          <FontAwesome name="user" size={50} color="black" />
        </View>
        <Text style={homeStyles.usernameText}>
          {advertismentStore.getAdvertismentOwner?.username || ''}
        </Text>
      </View>
      <Text style={homeStyles.sectionText}>Opis:</Text>
      <View style={homeStyles.descriptionView}>
        <Text style={homeStyles.text}>
          {advertismentStore.getAdvertismentOwner?.description || ''}
        </Text>
      </View>
      <Text style={homeStyles.sectionText}>Dane:</Text>
      <View style={homeStyles.dataView}>
        <Text style={homeStyles.text}>
          Imię: {advertismentStore.getAdvertismentOwner?.name || ''}
        </Text>
        <Text style={homeStyles.text}>
          Nazwisko: {advertismentStore.getAdvertismentOwner?.surname || ''}
        </Text>
        <Text style={homeStyles.text}>
          Email: {advertismentStore.getAdvertismentOwner?.email || ''}
        </Text>
      </View>
      <Text style={homeStyles.sectionText}>Ocena:</Text>
      <View style={homeStyles.raitingView}>
        <Text style={homeStyles.text}>Ocena społeczności :  </Text>
        <View style={homeStyles.starTextView}>
          <Text style={homeStyles.starText}>
            {advertismentStore.getAdvertismentOwner?.rating || 0}
          </Text>
        </View>
       
      </View>
      <View style={homeStyles.startView}>{RaintingStars()}</View>
    </ScrollView>
  );
};

export default observer(ProfileView);
