import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    paddingHorizontal: 15,
  },
  userPhoto: {
    height: 80,
    width: 80,
    backgroundColor: '#b8b8b880',
    borderRadius: 70,
    justifyContent: 'center',
    alignItems: 'center',
  },
  userHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
  },
  usernameText: {
    fontSize: 16,
    marginLeft: 5,
    fontWeight: '700',
  },
  text: {
    color: 'black',
    fontSize: 14,
  },

  descriptionView: {
    borderTopColor: '#b8b8b8',
    borderTopWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  sectionText: {
    fontSize: 14,
    fontWeight: '700',
    marginVertical:2,
    letterSpacing: 0.2,
  },
  dataView: {
    borderTopColor: '#b8b8b8',
    borderTopWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  startView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  raitingView: {
    borderTopColor: '#b8b8b8',
    borderTopWidth: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection:"row",
  },
  starTextView: {
    width: 60,
    height: 60,
    borderWidth: 2,
    borderRadius: 60,
    borderColor: '#fcd874',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 5,
  },
  starText: {
    fontSize: 22,
    fontWeight: '700',
    textAlign: 'center',
  },
});
