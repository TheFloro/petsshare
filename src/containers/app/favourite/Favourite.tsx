import {observer} from 'mobx-react-lite';
import React from 'react';
import {FlatList} from 'react-native';
import {useStores} from '../../../../App';
import AdvertismentCard from '../../../components/advertismentCard/AdvertismentCard';
import FundationCard from '../../../components/fundationCard/FundationCard';
const Favourite = () => {
  const {favouriteStore} = useStores().stores;
  return (
    <FlatList
      data={favouriteStore.favouriteList}
      keyExtractor={(item): any => item.id}
      contentContainerStyle={{alignItems: 'center'}}
      renderItem={item => {
        if (item.item.cachGoal) {
          return (
            <FundationCard
              id={item.item.id}
              title={item.item.title}
              cachGoal={item.item.cachGoal}
              cachColect={item.item.cachColect}
              foodGoal={item.item.foodGoal}
              foodColect={item.item.foodColect}
              otherGoal={item.item.otherGoal}
              otherColection={item.item.otherColection}
            />
          );
        } else {
          return (
            <AdvertismentCard
              id={item.item.id}
              title={item.item.title}
              price={item.item.price}
              description={item.item.description}
              image={item.item.image}
            />
          );
        }
      }}
    />
  );
};

export default observer(Favourite);
