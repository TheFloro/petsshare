import {observer} from 'mobx-react-lite';
import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {useStores} from '../../../App';
import homeStyles from './styles/homeStyles';
const RaintingStars = (raiting: number) => {
  const array = [];
  for (let i = 0; i < 5; i++) {
    if (i < raiting) {
      array.push(<FontAwesome name="star" size={30} color="#fcd874" key={i} />);
    } else {
      array.push(
        <FontAwesome name="star-o" size={30} color="#fcd874" key={i} />,
      );
    }
  }
  return array;
};
const Home = () => {
  const {authStore} = useStores().stores;
  console.log(authStore.user);
  return (
    <ScrollView style={homeStyles.container}>
      <View style={homeStyles.userHeader}>
        <View style={homeStyles.userPhoto}>
          <FontAwesome name="user" size={50} color="black" />
        </View>
        <Text style={homeStyles.usernameText}>
          {authStore.user?.username || ''}
        </Text>
      </View>
      <Text style={homeStyles.sectionText}>Opis:</Text>
      <View style={homeStyles.descriptionView}>
        <Text style={homeStyles.text}>{authStore.user?.description || ''}</Text>
      </View>
      <Text style={homeStyles.sectionText}>Dane:</Text>
      <View style={homeStyles.dataView}>
        <Text style={homeStyles.text}>Imię: {authStore.user?.name || ''}</Text>
        <Text style={homeStyles.text}>
          Nazwisko: {authStore.user?.surname || ''}
        </Text>
        <Text style={homeStyles.text}>
          Email: {authStore.user?.email || ''}
        </Text>
      </View>
      <Text style={homeStyles.sectionText}>Ocena:</Text>
      <View style={homeStyles.raitingView}>
        <View style={homeStyles.starTextView}>
          <Text style={homeStyles.starText}>{authStore.user?.rating || 0}</Text>
        </View>
        <View style={homeStyles.startView}>
          {RaintingStars(authStore.user?.rating || 0)}
        </View>
      </View>
    </ScrollView>
  );
};

export default observer(Home);
