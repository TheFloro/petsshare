import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#eaeaea',
  },
  advertismentImgae: {
    width: '100%',
    height: 300,
  },
  buttonTopContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  goBack: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 35,
    height: 35,
    borderRadius: 50,
    backgroundColor: 'white',
    marginTop: 10,
    marginLeft: 10,
  },
  favoutiteContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 35,
    height: 35,
    borderRadius: 50,
    backgroundColor: 'white',
    marginTop: 10,
    marginRight: 10,
  },
  infoContainer: {
    height: 300,
    paddingHorizontal: 30,
    display: 'flex',
    backgroundColor: 'white',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    elevation: 5,
    marginBottom: 20,
  },
  infoBox: {
    width: '80%',
    height: 80,
    display: 'flex',
    paddingVertical: 10,
  },
  title: {
    width: '100%',
    fontSize: 20,
    fontWeight: '600',
  },
  favourite: {
    color: 'red',
  },
  goalTitle: {
    fontSize: 18,
    fontWeight: '600',
    marginBottom: 20,
  },
  goalStatusContainer: {
    display: 'flex',
    flexDirection: 'row',
  },
  goalStatusBarContainer: {
    display: 'flex',
    width: '70%',
  },
  goalButtonsContainer: {
    width: '30%',
    marginTop: -5,
  },
  goalBar: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    height: 20,
    backgroundColor: '#48DA9F',
    marginBottom: 30,
    borderRadius: 20,
    elevation: 5,
  },
  goalStatus: {
    fontSize: 16,
    fontWeight: '600',
  },
  goalButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 30,
    fontSize: 12,
    marginBottom: 20,
    backgroundColor: '#48DA9F',
    borderRadius: 10,
    elevation: 5,
  },
  offertDescription: {
    height: 150,
    paddingHorizontal: 30,
    display: 'flex',
  },
  description: {
    marginTop: 15,
    fontSize: 15,
  },
  ownerContainer: {
    width: '100%',
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    height: 120,
    paddingHorizontal: 10,
  },
  ownerDetailsContainer: {
    display: 'flex',
    height: '100%',
    width: '70%',
    marginTop: 40,
    marginLeft: 10,
  },
  userPhoto: {
    height: 80,
    width: 80,
    backgroundColor: '#b8b8b880',
    borderRadius: 70,
    justifyContent: 'center',
    alignItems: 'center',
  },
  shelterName: {
    fontSize: 17,
    fontWeight: '600',
  },
  fundationInfo: {
    fontSize: 15,
    fontWeight: '600',
  },
  fundationContact:{
    
  }
});
