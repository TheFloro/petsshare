import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#eaeaea',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
});
