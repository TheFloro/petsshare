import React from 'react';
import {
  ImageBackground,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Linking,
} from 'react-native';
import fundationDetailsStyle from './styles/fundationDetailsStyle';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { useStores } from '../../../../App';
import { observer } from 'mobx-react-lite';

const FundationDetails = () => {
  const {navigationStore, fundationStore, favouriteStore} = useStores().stores;

  const fundation = fundationStore.getFundation;
  const fundationOwner = fundationStore.getFundationOwner;
  return (
    <ScrollView style={fundationDetailsStyle.container}>
      <ImageBackground
        source={require('../../../commons/images/loginscreen.jpg')}
        resizeMode="cover"
        style={fundationDetailsStyle.advertismentImgae}>
        <View style={fundationDetailsStyle.buttonTopContainer}>
          <TouchableOpacity style={fundationDetailsStyle.goBack}>
            <Ionicons
              name={'chevron-back-outline'}
              size={30}
              onPress={() => navigationStore.goBack()}
            />
          </TouchableOpacity>
          <TouchableOpacity style={fundationDetailsStyle.favoutiteContainer}>
            <Ionicons
              style={fundationDetailsStyle.favourite}
              name={
                favouriteStore.isFundationInFavourites
                  ? 'ios-heart'
                  : 'ios-heart-outline'
              }
              size={30}
              onPress={() => favouriteStore.setFavouriteToList('fundation')}
            />
          </TouchableOpacity>
        </View>
      </ImageBackground>
      <View style={fundationDetailsStyle.infoContainer}>
        <View style={fundationDetailsStyle.infoBox}>
          <Text style={fundationDetailsStyle.title}>{fundation.title}</Text>
        </View>
        <View>
          <Text style={fundationDetailsStyle.goalTitle}>Cele zbiórki</Text>
          <View style={fundationDetailsStyle.goalStatusContainer}>
            <View style={fundationDetailsStyle.goalStatusBarContainer}>
              <View style={{ width: '90%', height: 25, borderRadius: 20, backgroundColor: 'rgba(0,53,235,0.2)', elevation: 20, marginBottom: 22}}>
                <View style={{ width: `${(((fundation.cachColect * 100) / (fundation.cachGoal * 100)) * 100).toFixed(2) + '%'}`, height: '100%', borderRadius: 20, backgroundColor: '#48DA9F', justifyContent: 'center' }}>
                </View>
              </View>
              <View style={{ width: '90%', height: 25, borderRadius: 20, backgroundColor: 'rgba(0,53,235,0.2)', elevation: 20, marginBottom: 22 }}>
                <View style={{ width: `${(((fundation.foodColect * 100) / (fundation.foodGoal * 100)) * 100).toFixed(2) + '%'}`, height: '100%', borderRadius: 20, backgroundColor: '#48DA9F', justifyContent: 'center' }}>
                </View>
              </View>
              <View style={{ width: '90%', height: 25, borderRadius: 20, backgroundColor: 'rgba(0,53,235,0.2)', elevation: 20, marginBottom: 22 }}>
                <View style={{ width: `${(((fundation.otherColection * 100) / (fundation.otherGoal * 100)) * 100).toFixed(2) + '%'}`, height: '100%', borderRadius: 20, backgroundColor: '#48DA9F', justifyContent: 'center' }}>
                </View>
              </View>
              {/* <View style={fundationDetailsStyle.goalBar}>
                <Text style={fundationDetailsStyle.goalStatus}>
                  {(((fundation.otherColection * 100) / (fundation.otherGoal * 100)) * 100).toFixed(2) + '%'}
                </Text>
              </View> */}
            </View>
            <View style={fundationDetailsStyle.goalButtonsContainer}>
              <TouchableOpacity
                style={fundationDetailsStyle.goalButton}
                onPress={() => {
                  navigationStore.navigate('DotationMoreInfo', {
                    screen: 'DotationMoreInfo',
                    params: {
                      title: 'Dotacja pieniężna',
                      procent: (((fundation.cachColect * 100) / (fundation.cachGoal * 100)) * 100).toFixed(2),
                      description: 'Chcesz wspomóc te pupile pieniężnie? Ich opiekunowie będą mieli okazję przeznaczyć Twoją wpłatę na najważniejsze potrzeby!',
                      now: fundation.cachColect,
                      goal: `${fundation.cachGoal} zł`,
                      //photo: 
                    }
                  });
                }}>
                <Text>Dotacja</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={fundationDetailsStyle.goalButton}
                onPress={() => {
                  navigationStore.navigate('DotationMoreInfo', {
                    screen: 'DotationMoreInfo',
                    params: {
                      title: 'Kup Karme',
                      procent: (((fundation.foodColect * 100) / (fundation.foodGoal * 100)) * 100).toFixed(2),
                      description: 'Chcesz zafundować zwierzakom żywność? W schronisku jej nigdy mało!',
                      now: fundation.foodColect,
                      goal: `${fundation.foodGoal} kg`,
                      //photo: 
                    }
                  });
                }}>
                <Text>Kup karme</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={fundationDetailsStyle.goalButton}
                onPress={() => {
                  navigationStore.navigate('DotationMoreInfo', {
                    screen: 'DotationMoreInfo',
                    params: {
                      title: 'Kup przedmioty',
                      procent: (((fundation.otherColection * 100) / (fundation.otherGoal * 100)) * 100).toFixed(2),
                      description: 'Zwierzęta, również potrzebują zabawy, daj im szansę na pogryzienie jakiejś zabawki!',
                      now: fundation.otherColection,
                      goal: `${fundation.otherGoal} przedmiotów`
                      //photo: 
                    }
                  });
                }}>
                <Text>Kup rzeczy</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      <View style={fundationDetailsStyle.offertDescription}>
        <Text style={fundationDetailsStyle.title}>Opis</Text>
        <Text style={fundationDetailsStyle.description}>
          {fundation.description}
        </Text>
      </View>
      <View style={fundationDetailsStyle.ownerContainer}>
        <View style={fundationDetailsStyle.userPhoto}>
          <FontAwesome name="user" size={50} color="black" />
        </View>
        <View style={fundationDetailsStyle.ownerDetailsContainer}>
          <Text
            style={fundationDetailsStyle.shelterName}
            numberOfLines={1}
            ellipsizeMode="tail">
            {fundationOwner.name}
          </Text>
          <Text style={fundationDetailsStyle.fundationContact}>
            {fundationOwner.adres}
          </Text>
          <TouchableOpacity
            onPress={() => Linking.openURL(`mailto:${fundationOwner.email}`)}>
            <Text style={fundationDetailsStyle.fundationContact}>
              {fundationOwner.email}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => Linking.openURL(`tel:${fundationOwner.phone}`)}>
            <Text style={fundationDetailsStyle.fundationContact}>
              {fundationOwner.phone}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default observer(FundationDetails);
