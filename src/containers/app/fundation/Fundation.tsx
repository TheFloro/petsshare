import React, { useEffect } from 'react';
import { FlatList, Text, View } from 'react-native';
import fundationStyles from './styles/fundationStyles';
import FundationCard from '../../../components/fundationCard/FundationCard';
import { useStores } from '../../../../App';
import { observer } from 'mobx-react-lite';

const Fundation = () => {
  const { fundationStore } = useStores().stores;

  return (
      <FlatList
        data={fundationStore.fundationList}
        keyExtractor={({id}): any => id}
        key={fundationStore.fundationList.length}
        contentContainerStyle={{alignItems: 'center'}}
        renderItem={item => (
          <FundationCard
            id={item.item.id}
            title={item.item.title}
            cachGoal={item.item.cachGoal}
            cachColect={item.item.cachColect}
            foodGoal={item.item.foodGoal}
            foodColect={item.item.foodColect}
            otherGoal={item.item.otherGoal}
            otherColection={item.item.otherColection}
          />
        )}
      />
  );
};

export default observer(Fundation);
