import React from 'react';
import {
  ImageBackground,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Linking,
} from 'react-native';
import advertismentDetailsStyle from './styles/advertismentDetailsStyle';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {useStores} from '../../../../App';
import {observer} from 'mobx-react-lite';

const AdvertismentDetails = ({route, navigation}: any) => {
  const {
    id,
    title,
    description,
    shortDescription,
    price,
    location,
    image,
    user,
  } = route.params.params;
  const {navigationStore, advertismentStore, favouriteStore, authStore} =
    useStores().stores;

  const owner = advertismentStore.getAdvertismentOwner;

  const RaintingStars = (raiting: number) => {
    const array = [];
    for (let i = 0; i < 5; i++) {
      if (i < raiting) {
        array.push(
          <FontAwesome name="star" size={30} color="#fcd874" key={i} />,
        );
      } else {
        array.push(
          <FontAwesome name="star-o" size={30} color="#fcd874" key={i} />,
        );
      }
    }
    return array;
  };

  return (
    <ScrollView style={advertismentDetailsStyle.container}>
      <ImageBackground
        source={image}
        resizeMode="cover"
        style={advertismentDetailsStyle.advertismentImgae}>
        <View style={advertismentDetailsStyle.buttonTopContainer}>
          <TouchableOpacity style={advertismentDetailsStyle.goBack}>
            <Ionicons
              name={'chevron-back-outline'}
              size={30}
              onPress={() => navigationStore.goBack()}
            />
          </TouchableOpacity>
          <TouchableOpacity style={advertismentDetailsStyle.favoutiteContainer}>
            <Ionicons
              style={advertismentDetailsStyle.favourite}
              name={
                favouriteStore.isContentInFavourites
                  ? 'ios-heart'
                  : 'ios-heart-outline'
              }
              size={30}
              onPress={() => favouriteStore.setFavouriteToList('advertisment')}
            />
          </TouchableOpacity>
        </View>
      </ImageBackground>
      <View style={advertismentDetailsStyle.infoContainer}>
        <View style={advertismentDetailsStyle.infoBox}>
          <Text style={advertismentDetailsStyle.title}>{title}</Text>
          <View style={{marginTop: 15}}>
            <Text style={advertismentDetailsStyle.price}>Cena: {price}</Text>
            <Text style={advertismentDetailsStyle.price}>
              Lokalizacja: {location}
            </Text>
          </View>
        </View>
      </View>
      <View style={advertismentDetailsStyle.offertDescription}>
        <Text style={advertismentDetailsStyle.title}>Opis</Text>
        <Text style={advertismentDetailsStyle.description}>{description}</Text>
      </View>
      <View style={advertismentDetailsStyle.ownerContainer}>
        <View style={advertismentDetailsStyle.userPhoto}>
          <FontAwesome name="user" size={50} color="black" />
        </View>
        <View style={advertismentDetailsStyle.ownerDetailsContainer}>
          <Text
            style={advertismentDetailsStyle.userName}
            onPress={() => {
              if (owner?.id === authStore.user?.id) {
                navigationStore.navigate('Profile');
              } else {
                navigationStore.navigate('OtherPeopleProfile');
              }
            }}>
            {owner?.username || ''}
          </Text>
          <View style={advertismentDetailsStyle.startView}>
            {RaintingStars(owner?.rating || 0)}
            <Text style={advertismentDetailsStyle.ratingPoints}>
              {owner?.rating || 0}
            </Text>
          </View>
        </View>
        <View style={advertismentDetailsStyle.contactButtonContainer}>
          <TouchableOpacity
            style={advertismentDetailsStyle.contactButton}
            onPress={() => Linking.openURL(`tel:${owner?.phone || 666}`)}>
            <Text>Zadzwoń/SMS</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default observer(AdvertismentDetails);
