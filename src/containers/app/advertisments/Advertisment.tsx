import React, {useEffect} from 'react';
import {FlatList, Text, View} from 'react-native';
import advertismentStyles from './styles/advertismentStyles';
import AdvertismentCard from '../../../components/advertismentCard/AdvertismentCard';
import {advertismentData} from '../../../data/index';
import {useStores} from '../../../../App';
import {observer} from 'mobx-react-lite';

const Advertisment = () => {
  const {advertismentStore} = useStores().stores;

  return (
    <FlatList
      data={advertismentStore.advertismentList}
      keyExtractor={({id}): any => id}
      key={advertismentStore.advertismentList.length}
      contentContainerStyle={{alignItems: 'center'}}
      renderItem={item => (
        <AdvertismentCard
          id={item.item.id}
          title={item.item.title}
          description={item.item.description}
          sgirtDescription={item.item.shortDescription}
          price={item.item.price}
          location={item.item.location}
          image={item.item.image}
          user={item.item.user}
        />
      )}
    />
  );
};

export default observer(Advertisment);
