import {observer} from 'mobx-react-lite';
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Modal,
  Button,
  Image,
} from 'react-native';
import {useStores} from '../../../App';
import AddOfferButton from '../../commons/components/AddOfferButton';
import CustomModal from '../../commons/components/CustomModal';
import CustomTextInput from '../../commons/components/CustomTextInput';
import {MediaType} from 'react-native-image-picker';
import {launchImageLibrary} from 'react-native-image-picker';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const AddOfferScreenView = () => {
  const [modal, setModal] = useState(false);
  const {offerStore, navigationStore} = useStores().stores;
  const [response, setResponse] = React.useState<any>(null);

  useEffect(() => {
    offerStore.clear();
  }, [offerStore.fullData[1]]);

  const setSmthInModal = (fetchWith: string) => {
    if (fetchWith === 'pet') {
      offerStore.fetchRealArrayWithPets();
    }
    if (fetchWith === 'item') {
      offerStore.fetchRealArrayWithItems();
    }
    if (fetchWith === 'negotiation') {
      offerStore.fetchRealArrayWithNegotiation();
    }
    if (fetchWith === 'used') {
      offerStore.fetchRealArrayWithUsed();
    }
    if (fetchWith === 'localization') {
      offerStore.fetchRealArrayWithLocalization();
    }
    if (fetchWith === 'shipment') {
      offerStore.fetchRealArrayWithShipment();
    }
    setModal(true);
  };

  const mediaType: MediaType = 'photo';

  const takePhoto = () => {
    let options = {
      title: 'Pick The Picture',
      mediaType: mediaType,
      customButtons: [{name: 'PetShare', title: 'Choose Photo from...'}],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    launchImageLibrary(options, response => {
      setResponse(response);
      if (response.didCancel) {
        setResponse(null);
      }
      if (response.errorCode) {
        setResponse(null);
      }
    });
  };

  const submitOffer = () => {
    if (offerStore.fullData !== null && offerStore.fullData !== []) {
      if (offerStore.fullData.includes('')) {
      } else {
        offerStore.fetchAdvertismentData();
        navigationStore.navigate('Advertisment');
      }
    }
  };

  return (
    <>
      {modal && <CustomModal modalPress={() => setModal(false)} />}
      <ScrollView
        contentContainerStyle={{
          justifyContent: 'center',
          alignItems: 'center',
        }}
        style={{
          paddingVertical: 10,
          backgroundColor: '#eaeaea',
        }}>
        <View
          style={
            !response?.assets
              ? styles.pictureStyle
              : styles.pictureStyleHasImage
          }>
          <TouchableOpacity
            style={
              !response?.assets
                ? styles.buttonStyle
                : styles.buttonStyleHasImage
            }
            onPress={() => takePhoto()}>
            {response === null ? (
              <MaterialIcons
                name="add-photo-alternate"
                size={100}
                color="black"
              />
            ) : (
              <>
                {response?.assets &&
                  response?.assets.map(({uri}: any) => (
                    <View key={uri} style={styles.image}>
                      <Image
                        resizeMode="cover"
                        resizeMethod="scale"
                        style={{width: 245, height: 245}}
                        source={{uri: uri}}
                      />
                    </View>
                  ))}
              </>
            )}
          </TouchableOpacity>
        </View>
        <View style={styles.container}>
          <View style={styles.picers}>
            <CustomTextInput
              title="Dodaj tytuł"
              placeholder="Dodaj tytuł"
              value={offerStore.fullData[0]}
              keybord="default"
              onChangeText={(value: string) =>
                offerStore.onChangeText(value, 0)
              }
            />
            <AddOfferButton
              title="Wybierz zwierze"
              choosenSmth={offerStore.fullData[1]}
              buttonPress={() => setSmthInModal('pet')}
              titleVisibility={offerStore.fullData[1] === ''}
            />
            <AddOfferButton
              title="Wybierz kategorie"
              choosenSmth={offerStore.fullData[2]}
              buttonPress={() => setSmthInModal('item')}
              disabled={!(offerStore.fullData[1] !== '')}
              titleVisibility={offerStore.fullData[2] === ''}
            />
            <AddOfferButton
              title="Wybierz negocjacje"
              choosenSmth={offerStore.fullData[3]}
              buttonPress={() => setSmthInModal('negotiation')}
              titleVisibility={offerStore.fullData[3] === ''}
            />
            <AddOfferButton
              title="Używane/Nowe"
              choosenSmth={offerStore.fullData[4]}
              buttonPress={() => setSmthInModal('used')}
              titleVisibility={offerStore.fullData[4] === ''}
            />
            <AddOfferButton
              title="Lokalizacja"
              choosenSmth={offerStore.fullData[5]}
              buttonPress={() => setSmthInModal('localization')}
              titleVisibility={offerStore.fullData[5] === ''}
            />
            <AddOfferButton
              title="Dostawa"
              choosenSmth={offerStore.fullData[6]}
              buttonPress={() => setSmthInModal('shipment')}
              titleVisibility={offerStore.fullData[6] === ''}
            />
            <Text style={styles.text}>Opis</Text>
            <View style={styles.descInputView}>
              <TextInput
                value={offerStore.fullData[8]}
                style={styles.descTextInput}
                onChangeText={(value: string) =>
                  offerStore.onChangeText(value, 8)
                }
                multiline={true}
              />
            </View>
            <CustomTextInput
              title="Cena"
              placeholder="Cena"
              value={offerStore.fullData[7]}
              keybord="numeric"
              onChangeText={(value: string) =>
                offerStore.onChangeText(value, 7)
              }
            />
          </View>
        </View>
        <TouchableOpacity
          style={styles.buttonSaveStyle}
          onPress={() => submitOffer()}>
          <Text style={styles.buttonText}>Dodaj ogłoszenie</Text>
        </TouchableOpacity>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '90%',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 30,
    elevation: 10,
  },
  text: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: '600',
    marginVertical: 10,
  },
  pictureStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 250,
    width: '90%',
    //backgroundColor: 'lightgreen','#48DA9F'
    borderRadius: 25,
    overflow: 'hidden',
    marginHorizontal: 20,
    marginVertical: 20,
    marginTop: 20,
    elevation: 10,
  },
  pictureStyleHasImage: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 245,
    borderRadius: 25,
    marginHorizontal: 20,
    marginTop: 20,
    marginBottom: 20,
  },
  buttonStyle: {
    borderRadius: 25,
    backgroundColor: 'white',
    padding: 15,
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonStyleHasImage: {
    width: 245,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    overflow: 'hidden',
    elevation: 10,
  },
  descInputView: {
    borderRadius: 15,
    height: 150,
    width: 250,
    borderWidth: 1,
    alignItems: 'center',
  },
  descTextInput: {
    height: 135,
    width: 235,
    textAlignVertical: 'top',
  },
  buttonSaveStyle: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 300,
    height: 50,
    backgroundColor: '#48DA9F',
    borderRadius: 25,
    elevation: 10,
    marginVertical: 30,
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 17,
    letterSpacing: 1,
    fontWeight: '600',
    color: '#000',
  },
  picers: {
    margin: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    alignItems: 'center',
  },
});

export default observer(AddOfferScreenView);
