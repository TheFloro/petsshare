import {observer} from 'mobx-react-lite';
import React, {createRef, RefObject, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {useStores} from '../../../App';

const ProfileSettings = () => {
  const {authStore} = useStores().stores;
  const [visibility, setVisibility] = useState(false);
  const inputRef: RefObject<TextInput> = createRef();

  return (
    <View style={styles.container}>
      <View style={{alignItems: 'center'}}>
        <Text style={styles.logoutText}>Ustawienia profilu:</Text>
        <View
          style={{
            height: 200,
            alignItems: 'center',
            justifyContent: 'space-evenly',
          }}>
          <TouchableOpacity
            style={{}}
            onPress={() => setVisibility(!visibility)}>
            <Text>Pokaż hasło</Text>
          </TouchableOpacity>
          <View style={styles.password}>
            {visibility ? (
              <Text
                style={{
                  fontWeight: '700',
                  width: 100,
                  textAlign: 'center',
                  height: 20,
                }}>
                {authStore.password}
              </Text>
            ) : (
              <Text style={{fontWeight: '700', width: 100, height: 20}}></Text>
            )}
          </View>
          <TouchableOpacity
            style={{}}
            onPress={() => inputRef.current?.focus()}>
            <Text>Zmień hasło</Text>
          </TouchableOpacity>
          <View style={styles.password}>
            <TextInput ref={inputRef} style={{height: 20, width: 100}} />
          </View>
        </View>
      </View>
      <View style={{}}>
        <Text style={styles.logoutText}>Dodaj kartę:</Text>
      </View>
      <TouchableOpacity onPress={() => authStore.logout()}>
        <View style={styles.logoutContainer}>
          <Text style={styles.logoutText}>Wyloguj się</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  logoutContainer: {
    marginTop: 50,
    padding: 10,
    borderWidth: 1,
    borderRadius: 15,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,

    elevation: 15,
  },
  logoutText: {
    fontSize: 20,
    fontWeight: '900',
    color: '#000',
  },
  password: {
    padding: 10,
    borderWidth: 1,
    borderRadius: 15,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.43,
    shadowRadius: 9.51,

    elevation: 15,
  },
});

export default observer(ProfileSettings);
