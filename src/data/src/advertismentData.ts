import images from '../../commons/images';

export default [
  {
    id: 1,
    title: 'Buda',
    description:
      'Postanowiłem przechowywać pieski w domu, mam do wydania budę, dość używana. ',
    shortDescription: 'Sprzedam budę dla psa!',
    price: 99.99,
    location: 'Warszawa',
    image: images.dogHouse,
    user: 1,
  },
  {
    id: 2,
    title: 'Obroża',
    description:
      'Mój piesek wyrósł ze swojej obroży, więc chciałbym się jej pozbyć. Wystarczy odebrać.',
    shortDescription: 'Obroża dla pieska',
    price: 0.0,
    location: 'Warszawa',
    image: images.collar,
    user: 2,
  },
  {
    id: 3,
    title: 'Terrarium',
    description: 'Mam do wydania terrarrium dla Kameleona',
    shortDescription: 'Terrarrium do kupna!',
    price: 89.99,
    location: 'Gdańsk',
    image: images.terrarium,
    user: 1,
  },
  {
    id: 4,
    title: 'Akwarium',
    description:
      'Przeprowadzam rybki do większego akwarium, a nie chcę zaśmiecać sobie mieszkania :/',
    shortDescription: 'Akwarium do wydania',
    price: 0.0,
    location: 'Wrocław',
    image: images.aquarium,
    user: 2,
  },
  {
    id: 5,
    title: 'Legowisko dla kota',
    description: 'Chętnie odsprzedam jedno legowisko!',
    shortDescription: 'Legowisko szybko!',
    price: 49.99,
    location: 'Łódź',
    image: images.catbed,
    user: 1,
  },
  {
    id: 6,
    title: 'Zestaw',
    description:
      'Pozostała mi klatka, poidełko oraz dwie miseczki, chętnie wydam w dobre ręcę!',
    shortDescription: 'Zestaw dla małego gryzonia',
    price: 1.99,
    location: 'Opole',
    image: images.hamsters_cage,
    user: 2,
  },
  {
    id: 7,
    title: 'Karma dla gryzonii',
    description:
      'Zamówiłem troszeczkę za dużo, nie chcę aby się zmarnowało, a groszem nie śmierdzę, oddam pół darmo',
    shortDescription: '200kg karmy dla gryzonii',
    price: 199.99,
    location: 'Bydgoszcz',
    image: images.mouse_food,
    user: 1,
  },
];
