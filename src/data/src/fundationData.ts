export default [
  {
    id: 1000,
    title: 'Zbieranie na psy ze schroniska w Gdańsku',
    description: 'Zbieramy karmy, koce i pieniądze na schronisko w Gdańsku',
    shortDescription: 'Podolski',
    cachGoal: '100',
    cachColect: '40',
    foodGoal: '120',
    foodColect: '40',
    otherGoal: '200',
    otherColection: '32',
    location: 'Warszawa',
    image: 'fajnefoto',
    organization: 2,
  },
];
