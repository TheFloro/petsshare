export default [
  {
    id: 1,
    name: 'Schronisko w Olsztynie',
    shelterName: 'Olsztyńskie Schronisko',
    NIP: '2349294859',
    email: 'schronisko.dla.wszystkkich@gmail.com',
    password: 'shelter',
    adres: 'Olsztyn, ul.Długa 25',
    avatar: '',
    rating: 4,
    description:
      'Witam, jesteśmy Olsztyńskim schroniskiem, mamy u siebie wiele zwierząt, którym potrzeba wsparcia.',
    phone: 984320124,
  },
  {
    id: 2,
    name: 'Schronisko w Gdańsku',
    shelterName: 'Schronisko miejskie',
    NIP: '1343496559',
    email: 'our_shelter@gmail.com',
    password: 'shelter',
    adres: 'Gdańsk, ul.Chmielna 25',
    avatar: '',
    rating: 4,
    description:
      'Cześć, zajmujemy się głównie kotami oraz psami. Dodakowe Info: Codzienne wyprowadzanie piesków o godzinie 16.00!',
    phone: 684560123,
  },
];
