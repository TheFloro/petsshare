export {default as usersData} from './src/usersData';
export {default as advertismentData} from './src/advertismentData';
export {default as fundationData} from './src/fundationData';
export {default as shelterData} from './src/shelterData';

