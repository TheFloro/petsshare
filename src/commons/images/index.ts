export default {
  aquarium: require('./aquarium.jpg'),
  catbed: require('./catBed.jpg'),
  collar: require('./collar.jpg'),
  dogHouse: require('./dogHouse.jpg'),
  hamsters_cage: require('./Hamsters_cage.jpg'),
  mouse_food: require('./Mouse_food.jpg'),
  terrarium: require('./terrarrium.jpg'),
};
