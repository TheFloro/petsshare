import {UserInterface} from '../../types/index';

export class UserModel {
  name: string;
  surname: string;
  username: string;
  email: string;
  rating: number;
  avatar?: string;

  constructor(userData: UserInterface) {
    this.avatar = userData.avatar;
    this.name = userData.name;
    this.surname = userData.surname;
    this.username = userData.username;
    this.email = userData.email;
    this.rating = userData.rating;
  }
}
