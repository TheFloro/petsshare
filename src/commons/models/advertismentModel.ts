import {AdvertismentInterface} from '../../types/index';

export class AdvertismentModel {
  id: number;
  title: string;
  description: string;
  shortDescription: string;
  price: number;
  location: string;
  image: string;

  constructor(advertismentData: AdvertismentInterface) {
    this.id = advertismentData.id;
    this.title = advertismentData.title;
    this.description = advertismentData.description;
    this.shortDescription = advertismentData.shortDescription;
    this.price = advertismentData.price;
    this.location = advertismentData.location;
    this.image = advertismentData.image;
  }
}
