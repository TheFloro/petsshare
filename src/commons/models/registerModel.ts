import {UserModel} from './index';

export class resiterModel {
  user: UserModel;
  password: string;
  repeatePassword: string;

  constructor(registerData: any) {
    this.user = registerData.user;
    this.password = registerData.password;
    this.repeatePassword = registerData.repeatePassword;
  }
}
