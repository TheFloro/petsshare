import React, {useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import CustomModal from './CustomModal';

interface AddOfferButtonInterface {
  title: string;
  buttonPress: () => void;
  choosenSmth: string;
  disabled?: boolean;
  titleVisibility: boolean;
}

const AddOfferButton = ({
  title,
  buttonPress,
  choosenSmth,
  disabled,
  titleVisibility,
}: AddOfferButtonInterface) => {
  return (
    <>
      <TouchableOpacity
        style={[
          styles.particularPicer,
          disabled ? styles.particularInActive : styles.particularPicerActive,
        ]}
        onPress={buttonPress}
        disabled={disabled}>
        {titleVisibility ? (
          <Text style={styles.title}>{title}</Text>
        ) : (
          <Text style={styles.choosenPick}>{choosenSmth}</Text>
        )}
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  particularPicer: {
    margin: 10,
    padding: 10,
    borderRadius: 25,
    width: 200,
    height: 50,
    justifyContent: 'center',
    elevation: 5,
  },
  particularInActive: {
    width: 250,
    backgroundColor: '#cccccc',
  },
  particularPicerActive: {
    width: 250,
    backgroundColor: '#6be1b2',
  },
  title: {
    fontSize: 15,
    color: '#666666',
  },
  choosenPick: {
    fontSize: 17,
    fontWeight: '600',
    textAlign: 'center',
  },
});

export default AddOfferButton;
