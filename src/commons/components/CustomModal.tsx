import {observer} from 'mobx-react-lite';
import React from 'react';
import {
  View,
  Text,
  Modal,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import {useStores} from '../../../App';

interface CustomModalInterface {
  modalPress: () => void;
}

const CustomModal = ({modalPress}: CustomModalInterface) => {
  const {offerStore} = useStores().stores;

  const clickingOnSmth = (data: string) => {
    offerStore.fillTheData(data);
    modalPress();
  };

  return (
    <Modal transparent={true}>
      <View style={styles.container}>
        <TouchableOpacity
          style={{flex: 2}}
          onPress={modalPress}
          activeOpacity={1}
        />
        <View
          style={{
            flex: 4,
            marginHorizontal: 20,
            borderRadius: 25,
            backgroundColor: 'white',
            elevation: 10,
            overflow: 'hidden',
            paddingVertical: 15,
          }}>
          <FlatList
            contentContainerStyle={{
              flexGrow: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            data={offerStore.theRealArray}
            style={{}}
            keyExtractor={id => id}
            renderItem={item => (
              <TouchableOpacity
                style={styles.chooseAnimal}
                onPress={() => clickingOnSmth(item.item)}>
                <Text style={styles.chooseAnimalText}>{item.item}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
        <TouchableOpacity
          style={{flex: 2}}
          onPress={modalPress}
          activeOpacity={1}
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  chooseAnimal: {
    margin: 10,
    width: 250,
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#27be82',
    elevation: 10,
  },
  chooseAnimalText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '600',
    color: '#fff',
  },
});

export default observer(CustomModal);
