import { observer } from 'mobx-react-lite';
import React from 'react';
import { ImageBackground, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useStores } from '../../../App';
import advertismentDetailsStyle from '../../containers/app/advertisments/styles/advertismentDetailsStyle';

const DotationMoreInfo = ({ route, navigation }: any) => {
    const { navigationStore } = useStores().stores;
    const {
        title,
        procent,
        description,
        now,
        goal,
    } = route.params.params;

    return (
        <ScrollView style={advertismentDetailsStyle.container}>
            <ImageBackground
                source={require('../images/loginscreen.jpg')}
                resizeMode="cover"
                style={advertismentDetailsStyle.advertismentImgae}>
                <View style={advertismentDetailsStyle.buttonTopContainer}>
                    <TouchableOpacity style={advertismentDetailsStyle.goBack}>
                        <Ionicons
                            name={'chevron-back-outline'}
                            size={30}
                            onPress={() => navigationStore.goBack()}
                        />
                    </TouchableOpacity>
                </View>
            </ImageBackground>
            <View style={advertismentDetailsStyle.infoContainer}>
                <View style={[advertismentDetailsStyle.infoBox, { alignItems: 'center', width: '100%' }]}>
                    <Text style={[advertismentDetailsStyle.title, { textAlign: 'center' }]}>{title}</Text>
                    <Text style={{ fontWeight: '900' }}>{now}/{goal}</Text>
                    <View style={{ width: '100%', height: 25, borderRadius: 20, backgroundColor: 'rgba(0,53,235,0.2)', elevation: 20 }}>
                        <View style={{ width: `${procent}%`, height: 23, borderRadius: 20, backgroundColor: '#48DA9F', justifyContent: 'center' }}>
                            <Text style={{ textAlign: 'center', fontWeight: '700' }}>{procent}%</Text>
                        </View>
                    </View>
                </View>
            </View>
            <View style={advertismentDetailsStyle.offertDescription}>
                <Text style={advertismentDetailsStyle.title}>Opis</Text>
                <Text style={advertismentDetailsStyle.description}>{description}</Text>
            </View>
            <View style={{ alignItems: 'center', alignContent: 'center', justifyContent: 'center' }}>
                <TouchableOpacity style={{flexDirection: 'row', justifyContent:'space-around'}}>
                    <Text style={{ fontSize: 20 }}>Przejdź do płatności</Text>
                    <Ionicons name='cash-outline' size={30} />
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
};

export default observer(DotationMoreInfo);
