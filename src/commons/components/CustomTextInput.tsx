import React from 'react';
import {
  View,
  Text,
  Modal,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';

interface CustomInputInterface {
  title: string;
  keybord: 'default' | 'numeric';
  onChangeText: any;
  value: string;
  placeholder: string;
}

const CustomTextInput = ({
  title,
  keybord,
  onChangeText,
  value,
  placeholder,
}: CustomInputInterface) => {
  return (
    <View>
      <Text style={styles.text}>{title}</Text>
      <TextInput
        onChangeText={onChangeText}
        value={value}
        keyboardType={keybord}
        autoCapitalize="words"
        style={styles.textInput}
        placeholder={placeholder}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    textAlign: 'center',
    fontSize: 17,
    fontWeight: '600',
    marginTop: 10,
  },
  textInput: {
    width: 200,
    marginBottom: 10,
    borderBottomWidth: 1,
    fontSize: 15,
    fontWeight: '600',
  },
});

export default CustomTextInput;
