export interface UserInterface {
  id: number;
  avatar: string;
  name: string;
  surname: string;
  username: string;
  email: string;
  rating: number;
  description: string;
  phone:number;
}
