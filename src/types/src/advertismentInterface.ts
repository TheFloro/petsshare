export interface AdvertismentInterface {
  id: number;
  title: string;
  description: string;
  shortDescription: string;
  price: number;
  location: string;
  image?: any;
  user: number;
}
