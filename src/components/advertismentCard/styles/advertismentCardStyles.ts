import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  flatListContainer: {
    flex: 1,
  },
  container: {
    display: 'flex',
    width: '90%',
    height: 250,
    backgroundColor: 'white',
    borderRadius: 25,
    elevation: 10,
    marginVertical: 10,
    overflow: 'hidden',
  },
  imageContainer: {
    width: 360,
    height: 140,
    borderBottomWidth: 1,
    overflow: 'hidden',
  },
  infoContainer: {
    height: '40%',
    paddingHorizontal: 30,
    display: 'flex',
    flexDirection: 'row',
  },
  infoBox: {
    width: '80%',
    height: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  title: {
    width: '100%',
    fontSize: 20,
    fontWeight: '600',
  },
  favouriteContainer: {
    width: '20%',
    height: '100%',
    display: 'flex',
    alignItems: 'flex-end',
  },
  noFavourite: {
    color: 'red',
    width: 30,
    height: 30,
    marginTop: 10,
  },
  isFavourite: {
    color: 'red',
    width: 30,
    height: 30,
    marginTop: 10,
  },
  price: {
    marginTop: 15,
    fontWeight: '600',
    fontSize: 15,
  },
});
