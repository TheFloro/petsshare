import React, {useState} from 'react';
import {Dimensions, Image, Text, TouchableOpacity, View} from 'react-native';
import advertismentCardStyles from './styles/advertismentCardStyles';
import FontAwesome from 'react-native-vector-icons/Ionicons';
import {useStores} from '../../../App';
import {observer} from 'mobx-react-lite';

const AdvertismentCard = (props: any) => {
  const {navigationStore, advertismentStore, favouriteStore} =
    useStores().stores;
  const favourite = !!favouriteStore.favouriteList.find(x => x.id === props.id);
  const windowWidth = Dimensions.get('window').width;
  console.log(props);
  return (
    <TouchableOpacity
      style={advertismentCardStyles.container}
      onPress={() => {
        advertismentStore.setCurrentViewOffer(props.id);
        navigationStore.navigate('AdvertismentDetail', {
          screen: 'AdvertismentDetail',
          params: {
            id: props.id,
            title: props.title,
            description: props.description,
            shortDescription: props.shortDescription,
            price: props.price,
            location: props.location,
            image: props.image,
            user: props.user,
          },
        });
      }}>
      <View
        style={[
          advertismentCardStyles.imageContainer,
          {width: windowWidth - 40},
        ]}>
        <Image
          style={{width: 500, height: 200}}
          source={props.image}
          resizeMode="cover"
        />
      </View>
      <View style={advertismentCardStyles.infoContainer}>
        <View style={advertismentCardStyles.infoBox}>
          <Text style={advertismentCardStyles.title}>{props.title}</Text>
          <Text style={advertismentCardStyles.price}>Cena: {props.price}</Text>
        </View>
        <View style={advertismentCardStyles.favouriteContainer}>
          <FontAwesome
            style={
              favourite
                ? advertismentCardStyles.isFavourite
                : advertismentCardStyles.noFavourite
            }
            name={favourite ? 'ios-heart' : 'ios-heart-outline'}
            size={30}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default observer(AdvertismentCard);
