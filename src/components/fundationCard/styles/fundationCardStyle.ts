import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  flatListContainer: {
    flex: 1,
  },
  container: {
    display: 'flex',
    width: '90%',
    height: 350,
    backgroundColor: 'white',
    borderRadius: 25,
    elevation: 10,
    marginBottom: 20,
    overflow: 'hidden',
  },
  imageContainer: {
    width: '100%',
    height: 170,
    borderBottomWidth: 1,
    overflow: 'hidden',
  },
  fundationInfoContainer: {
    height: 200,
  },
  infoContainer: {
    height: 100,
    paddingHorizontal: 30,
    display: 'flex',
    flexDirection: 'row',
  },
  infoBox: {
    width: '80%',
    height: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  title: {
    width: '100%',
    fontSize: 20,
    fontWeight: '600',
  },
  favouriteContainer: {
    width: '20%',
    height: '100%',
    display: 'flex',
    alignItems: 'flex-end',
  },
  noFavourite: {
    color: 'red',
    width: 30,
    height: 30,
    marginTop: 10,
  },
  isFavourite: {
    color: 'red',
    width: 30,
    height: 30,
    marginTop: 10,
  },
  fundationGoals: {
    width: '100%',
    height: 50,
    paddingHorizontal: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  goalContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  goalTitle: {
    fontSize: 16,
    fontWeight: '600',
    marginBottom: 5,
  },
  goal: {
    width: 50,
    height: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    backgroundColor: '#48DA9F',
    elevation: 10,
  },
  summary: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 15,
    fontWeight: '600',
  },
});
