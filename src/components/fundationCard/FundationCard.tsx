import React, {useState} from 'react';
import {Dimensions, Image, Text, TouchableOpacity, View} from 'react-native';
import fundationCardStyle from './styles/fundationCardStyle';
import FontAwesome from 'react-native-vector-icons/Ionicons';
import {useStores} from '../../../App';
import {observer} from 'mobx-react-lite';

const FoundationCard = (props: any) => {
  const {navigationStore, fundationStore, favouriteStore} = useStores().stores;
  const windowWidth = Dimensions.get('window').width;
  const favourite = !!favouriteStore.favouriteList.find(x => x.id === props.id);
  return (
    <TouchableOpacity
      style={[
        fundationCardStyle.container,
        {width: windowWidth - 30, marginTop: 10},
      ]}
      onPress={() => {
        fundationStore.setCurrentViewFundation(props.id);
        navigationStore.navigate('FundationDetails');
      }}>
      <View style={fundationCardStyle.imageContainer}>
        <Image
          style={{width: 500, height: 200}}
          source={require('../../commons/images/loginscreen.jpg')}
        />
      </View>
      <View style={fundationCardStyle.fundationInfoContainer}>
        <View style={fundationCardStyle.infoContainer}>
          <View style={fundationCardStyle.infoBox}>
            <Text
              style={fundationCardStyle.title}
              numberOfLines={2}
              ellipsizeMode="tail">
              {props.title}
            </Text>
          </View>
          <View style={fundationCardStyle.favouriteContainer}>
            <FontAwesome
              style={
                favourite
                  ? fundationCardStyle.isFavourite
                  : fundationCardStyle.noFavourite
              }
              name={favourite ? 'ios-heart' : 'ios-heart-outline'}
              size={30}
            />
          </View>
        </View>
        <View style={fundationCardStyle.fundationGoals}>
          <View style={fundationCardStyle.goalContainer}>
            <Text style={fundationCardStyle.goalTitle}>Pieniądze</Text>
            <View style={fundationCardStyle.goal}>
              <Text style={fundationCardStyle.summary}>
                {(props.cachColect * props.cachGoal) / 100}%
              </Text>
            </View>
          </View>
          <View style={fundationCardStyle.goalContainer}>
            <Text style={fundationCardStyle.goalTitle}>Jedzenie</Text>
            <View style={fundationCardStyle.goal}>
              <Text style={fundationCardStyle.summary}>
                {(props.foodColect * props.foodGoal) / 100}%
              </Text>
            </View>
          </View>
          <View style={fundationCardStyle.goalContainer}>
            <Text style={fundationCardStyle.goalTitle}>Inne</Text>
            <View style={fundationCardStyle.goal}>
              <Text style={fundationCardStyle.summary}>
                {(props.otherColection * props.otherGoal) / 100}%
              </Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default observer(FoundationCard);
