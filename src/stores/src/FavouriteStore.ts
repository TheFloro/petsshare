import {BaseStore, RootStore} from '..';
import {computed, flow, makeObservable, observable} from 'mobx';
import {PersistDataStore} from './RootStore';
import AsyncStorage from '@react-native-async-storage/async-storage';
export default class FavouriteStore
  extends BaseStore
  implements PersistDataStore
{
  hydrateStore = async () => {
    const userJSON = await AsyncStorage.getItem('favourite');
    this.favouriteList = userJSON ? JSON.parse(userJSON) : this.favouriteList;
  };

  constructor(rootStore: RootStore) {
    super(rootStore);
    makeObservable(this, {
      favouriteList: observable,
      isContentInFavourites: computed,
      isFundationInFavourites: computed,
    });
  }

  favouriteList: Array<any> | [] = [];

  setFavouriteToList = flow(function* (
    this: FavouriteStore,
    type: 'advertisment' | 'fundation',
  ) {
    const {advertismentStore, fundationStore} = this.rootStore.stores;

    if (type === 'advertisment') {
      if (!advertismentStore.getAdvertisment) {
        return;
      }
      if (
        this.favouriteList.find(
          x => x.id === advertismentStore.getAdvertisment.id,
        )
      ) {
        const table = this.favouriteList;
        this.favouriteList = table.filter(
          x => x.id !== advertismentStore.getAdvertisment.id,
        );
      } else {
        this.favouriteList = [
          ...this.favouriteList,
          advertismentStore.getAdvertisment,
        ];
      }

      yield AsyncStorage.setItem(
        'favourite',
        JSON.stringify(this.favouriteList),
      );
    } else {
      if (!fundationStore.getFundation) {
        return;
      }
      if (
        this.favouriteList.find(x => x.id === fundationStore.getFundation.id)
      ) {
        const table = this.favouriteList;
        this.favouriteList = table.filter(
          x => x.id !== fundationStore.getFundation.id,
        );
      } else {
        this.favouriteList = [
          ...this.favouriteList,
          fundationStore.getFundation,
        ];
      }
      yield AsyncStorage.setItem(
        'favourite',
        JSON.stringify(this.favouriteList),
      );
    }
  }).bind(this);

  get isContentInFavourites() {
    const {advertismentStore} = this.rootStore.stores;
    if (!advertismentStore.getAdvertisment) {
      return false;
    }
    return !!this.favouriteList.find(
      x => x.id === advertismentStore.getAdvertisment.id,
    );
  }
  get isFundationInFavourites() {
    const {fundationStore} = this.rootStore.stores;
    if (!fundationStore.getFundation) {
      return false;
    }
    return !!this.favouriteList.find(
      x => x.id === fundationStore.getFundation.id,
    );
  }
}
