import {createRef, RefObject} from 'react';
import {BaseStore, RootStore} from '..';
import {makeObservable, observable} from 'mobx';
import {NavigationContainerRef} from '@react-navigation/core';
import {PersistDataStore} from './RootStore';

export default class NavigationStore
  extends BaseStore
  implements PersistDataStore
{
  hydrateStore = async () => {
    console.log('witam');
  };
  constructor(rootStore: RootStore) {
    super(rootStore);
    makeObservable(this, {
      navigationRef: observable.ref,
    });
  }

  navigationRef: RefObject<NavigationContainerRef<any>> = createRef();

  navigate = (routeName: string, params?: object): void => {
    if (!this.navigationRef.current) {
      return;
    }
    this.navigationRef.current.navigate(routeName, params);
  };

  goBack = () => {
    if (!this.navigationRef.current) {
      return;
    }

    this.navigationRef.current?.goBack();
  };
}
