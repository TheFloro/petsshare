import {BaseStore, RootStore} from '..';
import {comparer, computed, makeObservable, observable} from 'mobx';
import {PersistDataStore} from './RootStore';
import {fundationData, shelterData} from '../../data/index';

export default class FundationStore
  extends BaseStore
  implements PersistDataStore
{
  hydrateStore = async () => {
    console.log('witam');
  };

  constructor(rootStore: RootStore) {
    super(rootStore);
    makeObservable(this, {
      fundationList: observable,
      shelterList: observable,
      watchedFundationId: observable,
      getFundation: computed,
    });
  }

  fundationList: Array<any> | [] = [];
  shelterList: Array<any> | [] = [];
  watchedFundationId: number | null = null;

  getFundationList = () => {
    this.fundationList = fundationData;
  };

  getShelterList = () => {
    this.shelterList = shelterData;
  };

  setCurrentViewFundation = (id: number) => {
    this.watchedFundationId = id;
  };

  get getFundation() {
    const selectedFundation = this.fundationList.find(
      item => item.id === this.watchedFundationId,
    );
    return selectedFundation;
  }

  get getFundationOwner() {
    const fundationOwner = this.shelterList.find(
      item => item.id === this.getFundation?.organization,
    );
    return fundationOwner;
  }
}
