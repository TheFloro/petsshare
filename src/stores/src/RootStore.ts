import {
  AppStore,
  AuthStore,
  NavigationStore,
  OfferStore,
  AdvertismentStore,
  FundationStore,
  FavouriteStore,
  UsersStore,
} from '..';

export interface Stores {
  appStore: AppStore;
  authStore: AuthStore;
  navigationStore: NavigationStore;
  offerStore: OfferStore;
  advertismentStore: AdvertismentStore;
  fundationStore: FundationStore;
  favouriteStore: FavouriteStore;
  usersStore: UsersStore;
}

export interface PersistDataStore {
  hydrateStore: () => Promise<any>;
}
function implementsPersistDataStore(store: any): store is PersistDataStore {
  return 'hydrateStore' in store;
}
function persistDataStores(stores: Stores) {
  return Object.values(stores).filter(implementsPersistDataStore);
}

const stores = (store: RootStore): Stores => ({
  authStore: new AuthStore(store),
  navigationStore: new NavigationStore(store),
  appStore: new AppStore(store),
  offerStore: new OfferStore(store),
  advertismentStore: new AdvertismentStore(store),
  fundationStore: new FundationStore(store),
  favouriteStore: new FavouriteStore(store),
  usersStore: new UsersStore(store),
});
class RootStore {
  stores: Stores = stores(this);

  hydrateStores = () => {
    return Promise.all(
      persistDataStores(this.stores).map(store => store.hydrateStore()),
    );
  };
}
export default RootStore;
