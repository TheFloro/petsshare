import {BaseStore, RootStore} from '..';
import {action, flow, makeObservable} from 'mobx';
import SplashScreen from 'react-native-splash-screen';
export default class AppStore extends BaseStore {
  constructor(rootStore: RootStore) {
    super(rootStore);
    makeObservable(this, {
      appDidMount: action,
    });
  }
  appDidMount = flow(function* (this: AppStore) {
    const {hydrateStores} = this.rootStore;
    try {
      yield hydrateStores();
      setTimeout(() => {
        SplashScreen.hide();
      }, 500);
    } catch (error) {
      console.log(error);
    }
  }).bind(this);
}
