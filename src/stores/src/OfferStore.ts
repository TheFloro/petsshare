import {BaseStore, RootStore} from '..';
import {action, computed, flow, makeObservable, observable} from 'mobx';
import AsyncStorage from '@react-native-async-storage/async-storage';
import images from '../../commons/images';
interface AdvertismentData {
  id: number;
  photo?: any;
  title: string;
  pet: string;
  category: string;
  negotiation: string;
  used: string;
  localization: string;
  shipment: string;
  price: string;
}

interface AdvertismentInterface {
  id: number;
  title: string;
  description: string;
  shortDescription: string;
  price: string;
  location: string;
  image?: any;
  user: any;
}

export default class OfferStore extends BaseStore {
  constructor(rootStore: RootStore) {
    super(rootStore);
    makeObservable(this, {
      fullData: observable,
      dataForAdvertisment: observable,
      dataForRealReal: observable,

      theRealArray: observable,

      fetchRealArrayWithItems: action,
      fetchRealArrayWithPets: action,
      fetchRealArrayWithNegotiation: action,
      fetchRealArrayWithUsed: action,
      fetchRealArrayWithLocalization: action,
      fetchRealArrayWithShipment: action,
      fillTheData: action,
      onChangeText: action,
      clear: action,

      fetchAdvertismentData: action,
      clearCompletly: action,
    });
  }

  dataForAdvertisment: AdvertismentData[] = [
    {
      id: 1,
      //photo: ,
      title: 'TEST',
      pet: 'Dog',
      category: 'DogHouse',
      negotiation: 'Open to Negotiation',
      used: 'Used',
      localization: 'Wrocław',
      shipment: 'Dostawa',
      price: '16',
    },
  ];

  dataForRealReal: AdvertismentInterface[] = [
    {
      id: 1,
      title: 'Test',
      description: 'Opis haaaaaaaaaaaaaaaaaaaaaaaaaa',
      shortDescription: 'Opis',
      price: '100.00',
      location: 'Warszawa',
      //image:
      user: 2,
    },
    {
      id: 2,
      title: 'Test 1',
      description: 'Opis haaaaaaaaaaaaaaaaaaaaaaaaaa',
      shortDescription: 'Opis',
      price: '346',
      location: 'Gdańsk',
      //image:
      user: 3,
    },
    {
      id: 3,
      title: 'Test 2',
      description: 'Opis haaaaaaaaaaaaaaaaaaaaaaaaaa',
      shortDescription: 'Opis',
      price: '500',
      location: 'Wrocław',
      //image:
      user: 4,
    },
    {
      id: 4,
      title: 'Test 3',
      description: 'Opis haaaaaaaaaaaaaaaaaaaaaaaaaa',
      shortDescription: 'Opis',
      price: '130',
      location: 'Łódź',
      //image:
      user: 5,
    },
    {
      id: 5,
      title: 'Test 4',
      description: 'Opis haaaaaaaaaaaaaaaaaaaaaaaaaa',
      shortDescription: 'Opis',
      price: '120',
      location: 'Opole',
      //image:
      user: 6,
    },
    {
      id: 6,
      title: 'Test 5',
      description: 'Opis haaaaaaaaaaaaaaaaaaaaaaaaaa',
      shortDescription: 'Opis',
      price: '1',
      location: 'Bydgoszcz',
      //image:
      user: 7,
    },
  ];

  fullData: string[] = ['', '', '', '', '', '', '', '', '']; // 0 - title, 1-pet, 2-category, 3-negotia, 4-used, 5-localiza, 6-shipment, 7-price

  whichToFull: number | null = null;

  arrayOfPets: string[] = ['Pies', 'Kot', 'Gryzoń', 'Gady'];
  arrayOfNegotation: string[] = [
    'Otwarte na negocjacje',
    'Brak negocjacji',
    'Za darmo',
  ];
  arrayOfUsed: string[] = ['Nowe', 'Lekko używane', 'Używane', 'Mocno używane'];
  arrayOfLocalization: string[] = [
    'Warszawa',
    'Bydgoszcz',
    'Olsztyn',
    'Gdańsk',
    'Łódź',
    'Wrocław',
    'Poznań',
    'Other...',
  ];
  arrayofShipment: string[] = ['Paczkomat', 'Dostawa', 'Odbiór własny'];

  theRealArray: string[] = [];

  arrayForDogs: string[] = [
    'Legowisko',
    'Smycz',
    'Zabawka',
    'Obroża',
    'Miska',
    'Dekoracja',
    'Ubrania',
    'Buda',
    'Jedzenie',
  ];
  arrayForCats: string[] = [
    'Legowisko',
    'Miska',
    'Drapak',
    'Domek dla kota',
    'Zabawki',
    'Dekoracja',
    'Obroża',
    'Jedzenie',
  ];
  arrayForRodent: string[] = [
    'Pojemnik na wodę',
    'Kryjówka',
    'Miska',
    'Jedzenie',
    'Klatka',
    'Zabawka',
  ];
  arrayForReptiles: string[] = [
    'Pojemnik na wodę',
    'Jedzenie',
    'Terrarium',
    'Dekoracje',
    'Rośliny',
  ];

  fetchAdvertismentData = flow(function* (this: OfferStore) {
    const {advertismentStore, authStore} = this.rootStore.stores;
    advertismentStore.advertismentList.unshift({
      id: advertismentStore.advertismentList.length + 1,
      image: images.terrarium,
      title: this.fullData[0],
      location: this.fullData[5],
      description: this.fullData[8],
      shortDescription: '',
      price: Number(this.fullData[7]),
      user: authStore.user?.id || 0,
    });
    yield AsyncStorage.setItem(
      'advertisment',
      JSON.stringify(advertismentStore.advertismentList),
    );
    this.fullData = ['', '', '', '', '', '', '', '', ''];
  }).bind(this);

  fetchRealArrayWithPets = () => {
    this.theRealArray = this.arrayOfPets;
    this.whichToFull = 1;
  };
  fetchRealArrayWithItems = () => {
    if (this.fullData[1] === 'Pies') {
      this.theRealArray = this.arrayForDogs;
    }
    if (this.fullData[1] === 'Kot') {
      this.theRealArray = this.arrayForCats;
    }
    if (this.fullData[1] === 'Gryzoń') {
      this.theRealArray = this.arrayForRodent;
    }
    if (this.fullData[1] === 'Gady') {
      this.theRealArray = this.arrayForReptiles;
    }
    this.whichToFull = 2;
  };
  fetchRealArrayWithNegotiation = () => {
    this.theRealArray = this.arrayOfNegotation;
    this.whichToFull = 3;
  };
  fetchRealArrayWithUsed = () => {
    this.theRealArray = this.arrayOfUsed;
    this.whichToFull = 4;
  };
  fetchRealArrayWithLocalization = () => {
    this.theRealArray = this.arrayOfLocalization;
    this.whichToFull = 5;
  };
  fetchRealArrayWithShipment = () => {
    this.theRealArray = this.arrayofShipment;
    this.whichToFull = 6;
  };

  fillTheData = (data: string) => {
    if (this.whichToFull !== null) {
      this.fullData[this.whichToFull] = data;
    }
  };

  onChangeText = (value: string, index: number) => {
    this.fullData[index] = value;
  };

  clearCompletly = () => {
    this.fullData = ['', '', '', '', '', '', '', '', ''];
  };

  clear = () => {
    this.fullData[2] = '';
  };
}
