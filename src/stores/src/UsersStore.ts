import {BaseStore, RootStore} from '..';
import {comparer, computed, flow, makeObservable, observable} from 'mobx';
import {PersistDataStore} from './RootStore';
import {advertismentData, usersData} from '../../data/index';
import {AdvertismentInterface, UserInterface} from '../../types/index';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useStores} from '../../../App';
export default class UsersStore extends BaseStore implements PersistDataStore {
  hydrateStore = async () => {
    const userJSON = await AsyncStorage.getItem('userLists');
    if (!userJSON) {
      this.userList = usersData;
      await AsyncStorage.setItem('userLists', JSON.stringify(usersData));
    } else {
      this.userList = await JSON.parse(userJSON);
    }
  };

  constructor(rootStore: RootStore) {
    super(rootStore);
    makeObservable(this, {
      userList: observable,
    });
  }

  userList: Array<UserInterface> | [] = [];
}
