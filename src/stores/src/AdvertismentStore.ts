import {BaseStore, RootStore} from '..';
import {computed, makeObservable, observable} from 'mobx';
import {PersistDataStore} from './RootStore';
import {advertismentData} from '../../data/index';
import {AdvertismentInterface} from '../../types/index';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class AdvertismentStore
  extends BaseStore
  implements PersistDataStore
{
  hydrateStore = async () => {
    const userJSON = await AsyncStorage.getItem('advertisment');
    if (!userJSON) {
      this.advertismentList = advertismentData;
      await AsyncStorage.setItem(
        'advertisment',
        JSON.stringify(advertismentData),
      );
    } else {
      this.advertismentList = await JSON.parse(userJSON);
    }
  };

  constructor(rootStore: RootStore) {
    super(rootStore);
    makeObservable(this, {
      advertismentList: observable,
      watchedAdvertismentId: observable,
      getAdvertisment: computed,
      getAdvertismentOwner: computed,
    });
  }

  advertismentList: AdvertismentInterface[] = [];
  watchedAdvertismentId: number | null = null;

  setCurrentViewOffer = (id: number) => {
    this.watchedAdvertismentId = id;
  };

  get getAdvertisment() {
    const selectedAdvertisment = this.advertismentList.find(
      item => item.id === this.watchedAdvertismentId,
    );
    return selectedAdvertisment;
  }

  get getAdvertismentOwner() {
    const {usersStore} = this.rootStore.stores;
    const advertismentOwner = usersStore.userList.find(
      item => item.id === this.getAdvertisment?.user,
    );
    return advertismentOwner;
  }
}
