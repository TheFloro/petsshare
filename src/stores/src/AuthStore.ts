import {BaseStore, RootStore} from '..';
import {action, computed, makeObservable, observable} from 'mobx';
import {PersistDataStore} from './RootStore';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {usersData} from '../../data';
import {UserInterface} from '../../types';
export default class AuthStore extends BaseStore implements PersistDataStore {
  hydrateStore = async () => {
    const userJSON = await AsyncStorage.getItem('user');
    this.user = userJSON ? JSON.parse(userJSON) : this.user;
  };
  constructor(rootStore: RootStore) {
    super(rootStore);
    makeObservable(this, {
      user: observable,
      username: observable,
      password: observable,
      userLoggedIn: computed,
      setPassword: action,
      setUsername: action,
      logout: action,
    });
  }

  user: UserInterface | null = null;

  username = '';

  password = '';

  setPassword = (password: string) => {
    this.password = password;
  };
  setUsername = (name: string) => {
    this.username = name;
  };

  userLogin = () => {
    const account = usersData.find(item => item.name === this.username);
    if (account && account.password === this.password) {
      this.user = account;
      AsyncStorage.setItem('user', JSON.stringify(account));
    }
  };

  logout = () => {
    this.user = null;
  }

  get userLoggedIn() {
    return !!this.user;
  }
}
