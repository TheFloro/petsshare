import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    borderWidth: 1,
    borderRadius: 15,
    marginRight: 10,
    padding: 5,
  },
});
