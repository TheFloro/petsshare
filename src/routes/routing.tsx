import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import LoginScreen from '../containers/auth/LoginScreen';
import RegisterScreen from '../containers/auth/RegisterScreen';
import {observer} from 'mobx-react-lite';
import {useStores} from '../../App';
import TabBar from './TabBar';

const Stack = createNativeStackNavigator();
const MainNavigation = () => {
  const {navigationStore, authStore} = useStores().stores;
  console.log(authStore.userLoggedIn);
  return (
    <NavigationContainer
      ref={navigationStore.navigationRef}
      onStateChange={() => {}}>
      {authStore.userLoggedIn ? <AppNavigation /> : <LoginNavigation />}
    </NavigationContainer>
  );
};

const LoginNavigation = () => {
  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen
        name="Register"
        component={RegisterScreen}
        options={{
          headerTitle: 'Rejestracja',
          headerShown: true,
        }}
      />
    </Stack.Navigator>
  );
};

const AppNavigation = () => {
  return <TabBar />;
};

export default observer(MainNavigation);
