import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../containers/app/Home';
import Advertisment from '../containers/app/advertisments/Advertisment';
import AdvertismentDetails from '../containers/app/advertisments/AdvertismentDetails';
import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import tabBarStyles from './styles/tabBarStyles';
import {observer} from 'mobx-react-lite';
import {useStores} from '../../App';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AddOfferScreenView from '../containers/screens/AddOfferScreenView';
import FontAwesome from 'react-native-vector-icons/MaterialIcons';
import ProfileSettings from '../containers/screens/ProfileSettings';
import Fundation from '../containers/app/fundation/Fundation';
import FundationDetails from '../containers/app/fundation/FundationDetails';
import Favourite from '../containers/app/favourite/Favourite';
import DotationMoreInfo from '../commons/components/DotationMoreInfo';
import ProfileView from '../containers/app/profile/ProfileView';
const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const ProfileRouting = () => {
  const {navigationStore} = useStores().stores;
  return (
    <Stack.Navigator
      initialRouteName="Profile"
      screenOptions={{headerShown: true}}>
      <Stack.Screen
        name="Profile"
        component={Home}
        options={{
          headerTitle: 'Profil',
          headerRight: () => (
            <TouchableOpacity
              style={{
                width: 50,
                alignItems: 'center',
                justifyContent: 'center',
                height: 50,
              }}
              onPress={() => navigationStore.navigate('ProfileSettings')}>
              <FontAwesome name="settings" size={30} />
            </TouchableOpacity>
          ),
        }}
      />

      <Stack.Screen
        name="ProfileSettings"
        component={ProfileSettings}
        options={{headerTitle: 'Ustawienia'}}
      />
    </Stack.Navigator>
  );
};

const AdvertismentRouting = () => {
  const {navigationStore, advertismentStore} = useStores().stores;
  const name = advertismentStore.getAdvertismentOwner?.username || '';
  return (
    <Stack.Navigator
      initialRouteName="Advertisment"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Advertisment" component={Advertisment} />
      <Stack.Screen
        name="OtherPeopleProfile"
        component={ProfileView}
        options={{headerShown: true, headerTitle: `Profil ${name}`}}
      />
      <Stack.Screen name="AdvertismentDetail" component={AdvertismentDetails} />
    </Stack.Navigator>
  );
};

const FundationRouting = () => {
  return (
    <Stack.Navigator
      initialRouteName="Fundation"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Fundation" component={Fundation} />
      <Stack.Screen name="FundationDetails" component={FundationDetails} />
      <Stack.Screen name="DotationMoreInfo" component={DotationMoreInfo} />
    </Stack.Navigator>
  );
};

const FavouriteRoute = () => {
  return (
    <Stack.Navigator
      initialRouteName="Favourite"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Favourite" component={Favourite} />
      <Stack.Screen name="FundationDetails" component={FundationDetails} />
      <Stack.Screen name="AdvertismentDetail" component={AdvertismentDetails} />
    </Stack.Navigator>
  );
};

const TabBar = () => {
  return (
    <Tab.Navigator
      screenOptions={() => ({
        tabBarHideOnKeyboard: true,
        tabBarActiveTintColor: 'tomato',
        tabBarIcon: () => <FontAwesome name="folder" />,
        tabBarInactiveTintColor: 'gray',
        headerShown: false,
      })}>
      <Tab.Screen
        name="AdvertismentRouter"
        component={AdvertismentRouting}
        options={{
          tabBarLabel: 'Ogłoszenia',
          tabBarIcon: ({focused}) => (
            <FontAwesome
              name="shop"
              size={30}
              color={focused ? 'tomato' : 'gray'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="FundationRouter"
        component={FundationRouting}
        options={{
          tabBarLabel: 'Zbiórki',
          tabBarIcon: ({focused}) => (
            <FontAwesome
              name="foundation"
              size={30}
              color={focused ? 'tomato' : 'gray'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="AddOffer"
        component={AddOfferScreenView}
        options={{
          tabBarLabel: 'Dodaj',
          tabBarIcon: ({focused}) => (
            <FontAwesome
              name="add"
              size={30}
              color={focused ? 'tomato' : 'gray'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="ProfileRouter"
        component={ProfileRouting}
        options={{
          tabBarLabel: 'Profil',
          tabBarIcon: ({focused}) => (
            <FontAwesome
              name="face"
              size={30}
              color={focused ? 'tomato' : 'gray'}
            />
          ),
        }}
      />
      <Tab.Screen
        name="FavouriteRouter"
        component={FavouriteRoute}
        options={{
          tabBarLabel: 'Ulubione',
          tabBarIcon: ({focused}) => (
            <FontAwesome
              name="star"
              size={30}
              color={focused ? 'tomato' : 'gray'}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};
export default observer(TabBar);
